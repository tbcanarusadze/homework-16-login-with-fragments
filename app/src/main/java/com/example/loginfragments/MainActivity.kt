package com.example.loginfragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        addFragment(LogInFragment(), "log in fragment")
    }

    private fun init() {
        changeFragmentsBtn.setOnClickListener {
            if (changeFragmentsBtn.text.toString() == "Log In") {
                addFragment(LogInFragment(), "log in fragment")
                changeFragmentsBtn.text = "Sign Up"
            } else {
                addFragment(RegisterFragment(), "Register fragment")
                changeFragmentsBtn.text = "Log In"

            }

        }
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentLayout, fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commit()
    }
}
