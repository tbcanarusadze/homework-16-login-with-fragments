package com.example.loginfragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_register.view.*


class RegisterFragment : Fragment() {

    private lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_register, container, false)
        init()
        return itemView

    }

    private fun init() {
        itemView.signUpButton.setOnClickListener {
            checkEmpty()
        }
    }

    private fun signUp() {

        val parameters = mutableMapOf<String, String>()
        parameters["email"] = itemView.emailField.text.toString()
        parameters["password"] = itemView.passwordField2.text.toString()
        HttpRequest.postRequest(HttpRequest.REGISTRATION, parameters, object : CustomCallback {

            override fun onSuccess(response: String, message: String) {
                Toast.makeText(activity as MainActivity, message, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onError(response: String, message: String) {
                Toast.makeText(activity as MainActivity, message, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onFailure(response: String) {
                Toast.makeText(activity as MainActivity, "No Internet Connection", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun checkEmpty() {
        if (itemView.emailField.text.isNotEmpty() && itemView.passwordField2.text.isNotEmpty() && itemView.repeatPasswordField.text.isNotEmpty()) {
            checkPasswords()
        } else {
            Toast.makeText(activity as MainActivity, "please fill all fields!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkPasswords() {

        if (itemView.passwordField2.text.toString() != itemView.repeatPasswordField.text.toString()) {
            Toast.makeText(
                activity as MainActivity, "Passwords don't match", Toast.LENGTH_SHORT
            ).show()
        } else {
            signUp()
        }

    }

}
