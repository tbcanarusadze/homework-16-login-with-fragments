package com.example.loginfragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_log_in.view.*

class LogInFragment : Fragment(){

    private lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_log_in, container, false)
        init()
        return itemView
    }

    private fun init() {
        itemView.logInButton.setOnClickListener {
            checkEmpty()
        }

    }

    private fun checkEmpty() {
        if (itemView.emailField.text.isNotEmpty() && itemView.passwordField.text.isNotEmpty()) {
            logIn()
        } else {
            Toast.makeText(activity as MainActivity, "please fill all fields!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun logIn() {

        val parameters = mutableMapOf<String, String>()
        parameters["email"] = itemView.emailField.text.toString()
        parameters["password"] = itemView.passwordField.text.toString()
        HttpRequest.postRequest(HttpRequest.LOGIN, parameters, object : CustomCallback {

            override fun onSuccess(response: String, message:String) {
                Toast.makeText(activity as MainActivity, message, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onError(response: String, message:String) {
                Toast.makeText(activity as MainActivity, message, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onFailure(response: String) {
                Toast.makeText(activity as MainActivity, "No Internet Connection", Toast.LENGTH_SHORT)
                    .show()

            }
        })
    }



}
